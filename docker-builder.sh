#!/bin/bash

set -ex

CURRENT_DIR="${PWD##*/}"
IMAGE_NAME="$CURRENT_DIR"
TAG="0.0.1"

REGISTRY="williamleonpayu"

./mvnw clean package -U
docker login --username williamleonpayu --password "Wile)!06."
docker build -t ${REGISTRY}/${IMAGE_NAME}:${TAG} .
docker tag ${REGISTRY}/${IMAGE_NAME}:${TAG} ${REGISTRY}/${IMAGE_NAME}:latest
docker push ${REGISTRY}/${IMAGE_NAME}
