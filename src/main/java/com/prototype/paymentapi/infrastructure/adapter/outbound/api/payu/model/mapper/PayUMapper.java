package com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.model.mapper;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.prototype.paymentapi.domain.kernel.model.ImmutableResultTransaction;
import com.prototype.paymentapi.domain.kernel.model.Payment;
import com.prototype.paymentapi.domain.kernel.model.PurchaseOrder;
import com.prototype.paymentapi.domain.kernel.model.ResultTransaction;
import com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.model.AdditionalValue;
import com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.model.Address;
import com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.model.CreditCard;
import com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.model.Merchant;
import com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.model.MerchantCredentials;
import com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.model.Order;
import com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.model.PayURequest;
import com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.model.PayUResponse;
import com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.model.Transaction;
import org.springframework.util.DigestUtils;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public class PayUMapper {

	private static final String API_KEY = "4Vj8eK4rloUd272L48hsrarnUA";
	private static final String API_LOGIN = "pRRXKOl8ikMmt9u";
	private static final String ACCOUNT_ID = "512321";
	private static final String MERCHANT_ID = "508029";
	private static final String DEFAULT_CURRENCY = "COP";

	public static ResultTransaction fromResponse(PayUResponse response) {

		return ImmutableResultTransaction.builder()
										 .orderId(Optional.ofNullable(response.getTransactionResponse().getOrderId()))
										 .transactionId(Optional.ofNullable(response.getTransactionResponse().getTransactionId()))
										 .state(response.getTransactionResponse().getState())
										 .build();
	}

	public static PayURequest toApplyPayment(final Payment payment, final PurchaseOrder order,
											 final Optional<com.prototype.paymentapi.domain.kernel.command.payment.model.CreditCard> creditCard) {

		Address address = Address.builder()
								 .street1(payment.getUserAddress1())
								 .street2(payment.getUserAddress2().orElse(null))
								 .postalCode(payment.getPostalCode().orElse("000000"))
								 .city(payment.getUserCity())
								 .state(payment.getUserState())
								 .phone(payment.getUserContactPhone())
								 .build();

		Merchant buyer = Merchant.builder()
									.shippingAddress(address)
									.fullName(payment.getUserName())
									.contactPhone(payment.getUserContactPhone())
									.emailAddress(payment.getUserEmail())
									.dniNumber(payment.getUserDocument())
									.build();

		Merchant payer = Merchant.builder()
								 .billingAddress(address)
								 .fullName(payment.getUserName())
								 .contactPhone(payment.getUserContactPhone())
								 .emailAddress(payment.getUserEmail())
								 .dniNumber(payment.getUserDocument())
								 .build();

		Map<String, AdditionalValue> additionalValues = new HashMap<>();
		additionalValues.put("TX_VALUE", AdditionalValue.builder()
														.currency(DEFAULT_CURRENCY)
														.value(order.getCost().intValue())
														.build());

		return PayURequest.builder()
						  .merchant(getMerchantCredentials())
						  .transaction(Transaction.builder()
												  .type(payment.getTransactionType())
												  .paymentMethod(payment.getPaymentMethod())
												  .creditCardTokenId(payment.getCreditCardToken().orElse(null))
												  .creditCard(creditCard
																	  .map(c -> CreditCard.builder()
																						  .expirationDate(c.getExpirationDate())
																						  .name(c.getName())
																						  .number(c.getNumber())
																						  .securityCode(c.getSecurityCode())
																						  .build())
																	  .orElse(null))
												  .payer(payer)
												  .order(Order.builder()
															  .shippingAddress(address)
															  .buyer(buyer)
															  .accountId(ACCOUNT_ID)
															  .referenceCode(order.getId())
															  .description(String.format("Payment for %s", order.getId()))
															  .language("es")
															  .signature(getSignature(order.getId(), order.getCost().intValue()))
															  .additionalValues(additionalValues)
															  .build())
												  .build())
						  .build();
	}

	public static PayURequest toRevertPayment(final Payment payment) {

		return PayURequest.builder()
						  .merchant(getMerchantCredentials())
						  .transaction(Transaction.builder()
												  .order(Order.builder()
															  .id(payment.getTransactionOrderId().orElse(null))
															  .build())
												  .type(payment.getTransactionType())
												  .parentTransactionId(payment.getTransactionId().orElse(null))
												  .reason("Revert")
												  .build())
						  .build();
	}

	private static String getSignature(String referenceCode, Integer price) {

		return DigestUtils
				.md5DigestAsHex(
						String.format("%s~%s~%s~%s~%s", API_KEY, MERCHANT_ID, referenceCode, price, DEFAULT_CURRENCY)
							  .getBytes(StandardCharsets.UTF_8));
	}

	private static MerchantCredentials getMerchantCredentials() {

		return MerchantCredentials.builder()
								  .apiKey(API_KEY)
								  .apiLogin(API_LOGIN)
								  .build();
	}

}
