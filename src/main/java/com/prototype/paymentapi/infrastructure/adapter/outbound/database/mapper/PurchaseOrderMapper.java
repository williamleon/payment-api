package com.prototype.paymentapi.infrastructure.adapter.outbound.database.mapper;

import com.prototype.paymentapi.domain.kernel.model.ImmutablePurchaseOrder;
import com.prototype.paymentapi.domain.kernel.model.PurchaseOrder;
import com.prototype.paymentapi.infrastructure.adapter.outbound.database.model.DBPurchaseOrder;
import lombok.NoArgsConstructor;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@NoArgsConstructor
public class PurchaseOrderMapper {

	public static DBPurchaseOrder fromPurchaseOrder(PurchaseOrder po){
		return DBPurchaseOrder.builder()
							  .id(po.getId())
							  .reference(po.getReference())
							  .cost(po.getCost())
							  .status(po.getStatus())
							  .userId(po.getUserId())
							  .build();
	}

	public static PurchaseOrder fromDBPurchaseOrder(DBPurchaseOrder dbPo){
		return ImmutablePurchaseOrder.builder()
									 .id(dbPo.getId())
									 .reference(dbPo.getReference())
									 .cost(dbPo.getCost())
									 .status(dbPo.getStatus())
									 .userId(dbPo.getUserId())
									 .build();
	}
}
