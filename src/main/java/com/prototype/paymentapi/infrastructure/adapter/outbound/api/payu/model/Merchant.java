package com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.model;

import java.util.Optional;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Merchant {

	private String fullName;
	private String emailAddress;
	private String contactPhone;
	private String dniNumber;
	private Address billingAddress;
	private Address shippingAddress;
}
