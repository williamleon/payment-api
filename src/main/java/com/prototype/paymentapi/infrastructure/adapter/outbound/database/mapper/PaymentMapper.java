package com.prototype.paymentapi.infrastructure.adapter.outbound.database.mapper;

import java.util.Optional;

import com.prototype.paymentapi.domain.kernel.model.ImmutablePayment;
import com.prototype.paymentapi.domain.kernel.model.Payment;
import com.prototype.paymentapi.infrastructure.adapter.outbound.database.model.DBPayment;
import lombok.NoArgsConstructor;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@NoArgsConstructor
public class PaymentMapper {

	public static DBPayment fromPayment (Payment payment){
		return DBPayment.builder()
						.orderId(payment.getOrderId())
						.transactionStatus(payment.getTransactionStatus().orElse(null))
						.transactionOrderId(payment.getTransactionOrderId().orElse(null))
						.transactionType(payment.getTransactionType())
						.transactionId(payment.getTransactionId().orElse(null))
						.userEmail(payment.getUserEmail())
						.userName(payment.getUserName())
						.userContactPhone(payment.getUserContactPhone())
						.userDocument(payment.getUserDocument())
						.userAddress1(payment.getUserAddress1())
						.userAddress2(payment.getUserAddress2().orElse(null))
						.userCity(payment.getUserCity())
						.userState(payment.getUserState())
						.postalCode(payment.getPostalCode().orElse(null))
						.creditCardToken(payment.getCreditCardToken().orElse(null))
						.paymentMethod(payment.getPaymentMethod())
						.deviceSessionId(payment.getDeviceSessionId().orElse(null))
						.build();
	}

	public static Payment fromDBPayment(DBPayment payment){
		return ImmutablePayment.builder()
							   .id(payment.getId())
							   .orderId(payment.getOrderId())
							   .transactionStatus(payment.getTransactionStatus())
							   .transactionOrderId(payment.getTransactionOrderId())
							   .transactionType(payment.getTransactionType())
							   .transactionId(payment.getTransactionId())
							   .userEmail(payment.getUserEmail())
							   .userName(payment.getUserName())
							   .userContactPhone(payment.getUserContactPhone())
							   .userDocument(payment.getUserDocument())
							   .userAddress1(payment.getUserAddress1())
							   .userAddress2(Optional.ofNullable(payment.getUserAddress2()))
							   .userCity(payment.getUserCity())
							   .userState(payment.getUserState())
							   .postalCode(Optional.ofNullable(payment.getPostalCode()))
							   .creditCardToken(Optional.ofNullable(payment.getCreditCardToken()))
							   .paymentMethod(payment.getPaymentMethod())
							   .deviceSessionId(Optional.ofNullable(payment.getDeviceSessionId()))
							   .build();
	}

}
