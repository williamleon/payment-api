package com.prototype.paymentapi.infrastructure.adapter.outbound.stream;

import com.prototype.paymentapi.domain.kernel.event.payment.PaymentEvent;
import com.prototype.paymentapi.domain.usecase.stream.PaymentBinding;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class StreamPaymentBinding implements PaymentBinding {

	private final OutputBinding outputBinding;

	@Override public void send(final PaymentEvent event) {

		log.info(String.format("Sending event for payment %s", event.getPayment().getId()));
		outputBinding.paymentChannel()
					 .send(MessageBuilder
								   .withPayload(event)
								   .build());
	}
}
