package com.prototype.paymentapi.infrastructure.adapter.outbound.database.repository;

import static com.prototype.paymentapi.infrastructure.adapter.outbound.database.mapper.PurchaseOrderMapper.fromDBPurchaseOrder;
import static com.prototype.paymentapi.infrastructure.adapter.outbound.database.mapper.PurchaseOrderMapper.fromPurchaseOrder;

import java.util.Optional;

import com.prototype.paymentapi.domain.kernel.model.PurchaseOrder;
import com.prototype.paymentapi.domain.usecase.repository.PurchaseOrderRepository;
import com.prototype.paymentapi.infrastructure.adapter.outbound.database.mapper.PurchaseOrderMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class MapperPurchaseOrderRepository implements PurchaseOrderRepository {

	private final DBPurchaseOrderRepository repository;

	@Override public Optional<PurchaseOrder> findByIdAndStatus(final String id, final String status) {

		return repository.findByIdAndStatus(id, status)
						 .map(PurchaseOrderMapper::fromDBPurchaseOrder);
	}

	@Override public PurchaseOrder save(final PurchaseOrder purchaseOrder) {

		return fromDBPurchaseOrder(repository.save(fromPurchaseOrder(purchaseOrder)));
	}
}
