package com.prototype.paymentapi.infrastructure.adapter.inbound.controller;

import static com.prototype.paymentapi.infrastructure.adapter.inbound.controller.ResponseUtil.getMessage;

import javax.validation.Valid;

import com.prototype.paymentapi.domain.kernel.command.CommandBus;
import com.prototype.paymentapi.domain.kernel.command.payment.ApplyPaymentCommand;
import com.prototype.paymentapi.domain.kernel.model.Payment;
import com.prototype.paymentapi.domain.kernel.model.PurchaseOrder;
import io.vavr.control.Either;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/payment")
public class PaymentController {

	private final CommandBus commandBus;

	@PostMapping
	public ResponseEntity<?> create(@Valid @RequestBody ApplyPaymentCommand command) {

		Either<Throwable, Payment> execution = commandBus.dispatch(command);
		return execution.map(ResponseEntity::ok)
						.getOrElseGet(throwable -> new ResponseEntity(getMessage(throwable.getMessage()), HttpStatus.CONFLICT));
	}


}
