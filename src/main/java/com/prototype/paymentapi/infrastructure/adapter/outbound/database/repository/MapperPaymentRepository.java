package com.prototype.paymentapi.infrastructure.adapter.outbound.database.repository;

import static com.prototype.paymentapi.infrastructure.adapter.outbound.database.mapper.PaymentMapper.fromDBPayment;
import static com.prototype.paymentapi.infrastructure.adapter.outbound.database.mapper.PaymentMapper.fromPayment;

import java.util.Optional;

import com.prototype.paymentapi.domain.kernel.model.Payment;
import com.prototype.paymentapi.domain.usecase.repository.PaymentRepository;
import com.prototype.paymentapi.infrastructure.adapter.outbound.database.mapper.PaymentMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class MapperPaymentRepository implements PaymentRepository {

	private final DBPaymentRepository repository;

	@Override public Optional<Payment> findByOrderId(final String orderId) {

		return repository.findByOrderId(orderId)
						 .map(PaymentMapper::fromDBPayment);
	}

	@Override public Payment save(final Payment payment) {

		return fromDBPayment(repository.save(fromPayment(payment)));
	}
}
