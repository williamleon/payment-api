package com.prototype.paymentapi.infrastructure.adapter.inbound.stream.listener;

import com.prototype.paymentapi.domain.kernel.command.CommandBus;
import com.prototype.paymentapi.domain.kernel.command.order.UpdatePurchaseOrderCommand;
import com.prototype.paymentapi.domain.kernel.command.payment.RevertPaymentByPurchaseOrderCommand;
import com.prototype.paymentapi.infrastructure.adapter.inbound.stream.InputBinding;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

/**
 * Listener for purchase order event
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class PurchaseOrderListener {

	public final CommandBus bus;

	@StreamListener(target = InputBinding.PURCHASE_ORDER_MESSAGE)
	public void updatePurchaseOrder(UpdatePurchaseOrderCommand command) {

		log.info(String.format("Listen update order event for %s", command.getOrder().getId()));
		bus.dispatch(command);
	}

	@StreamListener(target = InputBinding.PURCHASE_ORDER_MESSAGE)
	public void revertPaymentByOrder(RevertPaymentByPurchaseOrderCommand command) {

		log.info(String.format("Listen revert payment event for %s", command.getOrder().getId()));
		bus.dispatch(command);
	}
}
