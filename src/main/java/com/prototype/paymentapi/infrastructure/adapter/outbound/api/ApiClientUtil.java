package com.prototype.paymentapi.infrastructure.adapter.outbound.api;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Component
public class ApiClientUtil {

	public Retrofit getClient(String baseUrl) {

		HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
		interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
		OkHttpClient client = new OkHttpClient.Builder()
				.addInterceptor(interceptor)
				.build();

		return new Retrofit.Builder()
				.baseUrl(baseUrl)
				.addConverterFactory(JacksonConverterFactory
											 .create(new ObjectMapper()
															 .setSerializationInclusion(JsonInclude.Include.NON_NULL)
															 .setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)
															 .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
													))
				.client(client)
				.build();
	}
}
