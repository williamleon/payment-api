package com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PayURequest {

	@Builder.Default
	private String language = "es";

	@Builder.Default
	private String command = "SUBMIT_TRANSACTION";

	@Builder.Default
	private Boolean test = true;

	private MerchantCredentials merchant;

	private Transaction transaction;

}
