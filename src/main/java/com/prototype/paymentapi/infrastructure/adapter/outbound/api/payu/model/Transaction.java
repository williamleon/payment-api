package com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.model;

import java.util.Optional;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {

	private Order order;
	private Merchant payer;
	private String creditCardTokenId;
	private CreditCard creditCard;
	private String type;
	private String paymentMethod;
	private String parentTransactionId;
	private String reason;
	@Builder.Default
	private String paymentCountry = "CO";
	@Builder.Default
	private String deviceSessionId = "vghs6tvkcle931686k1900o6e1";
	@Builder.Default
	private String ipAddress = "127.0.0.1";
	@Builder.Default
	private String cookie = "pt1t38347bs6jc9ruv2ecpv7o2";
	@Builder.Default
	private String userAgent = "Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0";

}
