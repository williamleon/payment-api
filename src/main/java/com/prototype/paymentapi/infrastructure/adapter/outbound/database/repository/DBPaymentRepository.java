package com.prototype.paymentapi.infrastructure.adapter.outbound.database.repository;

import java.util.Optional;

import com.prototype.paymentapi.infrastructure.adapter.outbound.database.model.DBPayment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Repository
public interface DBPaymentRepository extends JpaRepository<DBPayment, String> {

	Optional<DBPayment> findByOrderId(String orderId);

}
