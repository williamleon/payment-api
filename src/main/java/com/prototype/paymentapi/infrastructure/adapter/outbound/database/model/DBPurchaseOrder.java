package com.prototype.paymentapi.infrastructure.adapter.outbound.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Data
@Entity
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "purchase_orders", uniqueConstraints = {
		@UniqueConstraint(columnNames = "reference")
})
public class DBPurchaseOrder {

	@Id
	private String id;

	@Column(nullable = false)
	private Integer reference;

	@Column(nullable = false)
	private String userId;

	@Column(nullable = false)
	private String status;

	@Column(nullable = false)
	private Double cost;
}
