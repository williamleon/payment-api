package com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AdditionalValue {

	private Integer value;
	private String currency;
}
