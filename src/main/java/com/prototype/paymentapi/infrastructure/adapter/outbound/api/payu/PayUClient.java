package com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu;

import java.io.IOException;

import com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.model.PayURequest;
import com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.model.PayUResponse;
import retrofit2.Callback;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface PayUClient extends Callback<PayUResponse> {

	PayUResponse call(PayURequest request) throws IOException;

}
