package com.prototype.paymentapi.infrastructure.adapter.outbound.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Data
@Entity
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "payments")
public class DBPayment {

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	private String id;

	@Column(nullable = false)
	private String orderId;

	@Column(nullable = false)
	private String transactionStatus;

	@Column(nullable = false)
	private String transactionOrderId;

	@Column(nullable = false)
	private String transactionType;

	@Column(nullable = false)
	private String transactionId;

	@Column(nullable = false)
	private String userEmail;

	@Column(nullable = false)
	private String userName;

	@Column(nullable = false)
	private String userContactPhone;

	@Column(nullable = false)
	private String userDocument;

	@Column(nullable = false)
	private String userAddress1;

	private String userAddress2;

	@Column(nullable = false)
	private String userCity;

	@Column(nullable = false)
	private String userState;

	private String postalCode;

	private String creditCardToken;

	private String paymentMethod;

	private String deviceSessionId;

}
