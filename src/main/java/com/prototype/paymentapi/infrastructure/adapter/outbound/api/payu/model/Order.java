package com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.model;

import java.util.Map;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Order {

	private String id;
	private String accountId;
	private String referenceCode;
	private String description;
	private String language;
	private String signature;
	private String notifyUrl;
	private Map<String, AdditionalValue> additionalValues;
	private Merchant buyer;
	private Address shippingAddress;

}
