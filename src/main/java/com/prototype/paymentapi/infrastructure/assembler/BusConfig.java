package com.prototype.paymentapi.infrastructure.assembler;

import com.prototype.paymentapi.domain.kernel.command.CommandExecute;
import com.prototype.paymentapi.domain.kernel.command.order.ImmutableUpdatePurchaseOrderCommand;
import com.prototype.paymentapi.domain.kernel.command.payment.ImmutableApplyPaymentCommand;
import com.prototype.paymentapi.domain.kernel.command.payment.ImmutableRevertPaymentByPurchaseOrderCommand;
import com.prototype.paymentapi.domain.kernel.event.EventHandler;

import com.prototype.paymentapi.domain.kernel.event.payment.ImmutablePaymentEvent;
import com.prototype.paymentapi.domain.usecase.LocalCommandBus;
import com.prototype.paymentapi.domain.usecase.LocalEventBus;
import io.vavr.collection.HashMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure Query/Command bus
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Configuration
public class BusConfig {

	@Bean
	public LocalCommandBus commandBus(CommandExecute updatePurchaseOrderCommandExecute,
									  CommandExecute applyPaymentCommandExecute,
									  CommandExecute revertPaymentByPurchaseOrderCommandExecute) {

		return new LocalCommandBus(HashMap.of(ImmutableUpdatePurchaseOrderCommand.class.getName(), updatePurchaseOrderCommandExecute,
											  ImmutableApplyPaymentCommand.class.getName(), applyPaymentCommandExecute,
											  ImmutableRevertPaymentByPurchaseOrderCommand.class.getName(), revertPaymentByPurchaseOrderCommandExecute));
	}

	@Bean
	public LocalEventBus eventBus(EventHandler paymentEventHandler) {

		return new LocalEventBus(HashMap.of(ImmutablePaymentEvent.class.getName(), paymentEventHandler));
	}
}
