package com.prototype.paymentapi.infrastructure.assembler.usecase.service;

import static com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.model.mapper.PayUMapper.toApplyPayment;
import static com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.model.mapper.PayUMapper.toRevertPayment;

import java.util.Optional;

import com.prototype.paymentapi.domain.kernel.command.payment.model.CreditCard;
import com.prototype.paymentapi.domain.kernel.exception.TransactionException;
import com.prototype.paymentapi.domain.kernel.model.Payment;
import com.prototype.paymentapi.domain.kernel.model.PurchaseOrder;
import com.prototype.paymentapi.domain.kernel.model.ResultTransaction;
import com.prototype.paymentapi.domain.usecase.service.TransactionService;
import com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.PayUClient;
import com.prototype.paymentapi.infrastructure.adapter.outbound.api.payu.model.mapper.PayUMapper;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class PayUTransactionService implements TransactionService {

	private static final String MESSAGE_APPLY = "Getting error applying payment for purchase order %s";
	private static final String MESSAGE_REVERT = "Getting error reverting payment for purchase order %s";
	private final PayUClient applyPaymentCall;
	private final PayUClient revertPaymentCall;

	@Override public ResultTransaction apply(final Payment payment, final PurchaseOrder order, final Optional<CreditCard> creditCard) {

		log.info(String.format("Starting apply payment transaction for purchase order %s", order.getId()));
		return Try.of(() -> toApplyPayment(payment, order, creditCard))
				  .transform(payURequests -> Try.of(() -> applyPaymentCall.call(payURequests.get())))
				  .map(PayUMapper::fromResponse)
				  .filter(resultTransaction -> !"DECLINED".equals(resultTransaction.getState()))
				  .onFailure(throwable -> log.error(String.format(MESSAGE_APPLY, order.getId()), throwable))
				  .getOrElseThrow(throwable -> new TransactionException(throwable.getMessage()));
	}

	@Override public ResultTransaction revert(final Payment payment) {

		log.info(String.format("Starting revert payment transaction [%s] for purchase order %s", payment.getTransactionId(),
							   payment.getOrderId()));
		return Try.of(() -> toRevertPayment(payment))
				  .transform(payURequests -> Try.of(() -> revertPaymentCall.call(payURequests.get())))
				  .map(PayUMapper::fromResponse)
				  .onFailure(throwable -> log.error(String.format(MESSAGE_REVERT, payment.getOrderId()), throwable))
				  .getOrElseThrow(throwable -> new TransactionException(throwable.getMessage()));
	}
}
