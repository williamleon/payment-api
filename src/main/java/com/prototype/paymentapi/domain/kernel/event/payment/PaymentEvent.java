package com.prototype.paymentapi.domain.kernel.event.payment;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prototype.paymentapi.domain.kernel.event.Event;
import com.prototype.paymentapi.domain.kernel.model.Payment;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */

@Value.Style(get = {"is*", "get*"})
@JsonDeserialize(as = ImmutablePaymentEvent.class,  builder = ImmutablePaymentEvent.Builder.class)
@JsonSerialize(as = ImmutablePaymentEvent.class)
@Value.Immutable
public interface PaymentEvent extends Event {

	Payment getPayment();

}
