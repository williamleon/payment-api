package com.prototype.paymentapi.domain.kernel.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Style(get = {"is*", "get*"})
@Value.Immutable
@JsonDeserialize(as = ImmutablePurchaseOrder.class)
@JsonSerialize(as = ImmutablePurchaseOrder.class)
public interface PurchaseOrder {

	String getId();

	Integer getReference();

	String getUserId();

	String getStatus();

	Double getCost();
}
