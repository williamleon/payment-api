package com.prototype.paymentapi.domain.kernel.exception;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public class PaymentException extends RuntimeException {

	public PaymentException(String message) {

		super(message);
	}
}
