package com.prototype.paymentapi.domain.kernel.model;

import java.util.Optional;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Style(get = {"is*", "get*"})
@JsonDeserialize(as = ImmutablePayment.class, builder = ImmutablePayment.Builder.class)
@JsonSerialize(as = ImmutablePayment.class)
@Value.Immutable
public interface Payment {

	Optional<String> getId();

	String getOrderId();

	Optional<String> getTransactionStatus();

	Optional<String> getTransactionOrderId();

	Optional<String> getTransactionId();

	String getTransactionType();

	String getUserEmail();

	String getUserName();

	String getUserContactPhone();

	String getUserDocument();

	String getUserAddress1();

	Optional<String> getUserAddress2();

	String getUserCity();

	String getUserState();

	Optional<String> getPostalCode();

	Optional<String> getCreditCardToken();

	String getPaymentMethod();

	Optional<String> getDeviceSessionId();
}
