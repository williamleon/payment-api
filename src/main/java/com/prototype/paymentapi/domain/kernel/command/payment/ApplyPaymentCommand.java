package com.prototype.paymentapi.domain.kernel.command.payment;

import java.util.Optional;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prototype.paymentapi.domain.kernel.command.Command;
import com.prototype.paymentapi.domain.kernel.command.payment.model.CreditCard;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Style(get = {"is*", "get*"})
@JsonDeserialize(as = ImmutableApplyPaymentCommand.class)
@JsonSerialize(as = ImmutableApplyPaymentCommand.class)
@Value.Immutable
public interface ApplyPaymentCommand extends Command {

	String getOrderId();

	@Email
	String getUserEmail();

	@Size(max = 150)
	String getUserName();

	@Size(max = 11)
	String getUserContactPhone();

	@Size(max = 20)
	String getUserDocument();

	@Size(max = 100)
	String getUserAddress1();

	Optional<@Size(max = 100) String> getUserAddress2();

	@Size(max = 50)
	String getUserCity();

	@Size(max = 40)
	String getUserState();

	Optional<@Size(max = 8) String> getPostalCode();

	Optional<String> getCreditCardToken();

	Optional<CreditCard> getCreditCard();

	@Size(max = 32)
	String getPaymentMethod();

	Optional<String> getDeviceSessionId();
}
