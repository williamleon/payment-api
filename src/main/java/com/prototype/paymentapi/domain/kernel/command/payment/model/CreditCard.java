package com.prototype.paymentapi.domain.kernel.command.payment.model;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Style(get = {"is*", "get*"})
@JsonDeserialize(as = ImmutableCreditCard.class, builder = ImmutableCreditCard.Builder.class)
@JsonSerialize(as = ImmutableCreditCard.class)
@Value.Immutable
public interface CreditCard {

	@Size(max = 20)
	String getNumber();
	@Size(max = 4)
	String getSecurityCode();
	@Size(max = 7)
	String getExpirationDate();
	@Size(max = 255)
	String getName();
}
