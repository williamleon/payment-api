package com.prototype.paymentapi.domain.kernel.command.order;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prototype.paymentapi.domain.kernel.command.Command;
import com.prototype.paymentapi.domain.kernel.command.shared.model.PurchaseOrder;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Style(get = {"is*", "get*"})
@JsonDeserialize(as = ImmutableUpdatePurchaseOrderCommand.class)
@JsonSerialize(as = ImmutableUpdatePurchaseOrderCommand.class)
@Value.Immutable
public interface UpdatePurchaseOrderCommand extends Command {

	PurchaseOrder getOrder();
}
