package com.prototype.paymentapi.domain.kernel.model.mapper;

import com.prototype.paymentapi.domain.kernel.command.order.UpdatePurchaseOrderCommand;
import com.prototype.paymentapi.domain.kernel.command.payment.ApplyPaymentCommand;
import com.prototype.paymentapi.domain.kernel.model.ImmutablePayment;
import com.prototype.paymentapi.domain.kernel.model.ImmutablePurchaseOrder;
import com.prototype.paymentapi.domain.kernel.model.Payment;
import com.prototype.paymentapi.domain.kernel.model.PurchaseOrder;
import lombok.NoArgsConstructor;

/**
 * Mapper util for business objects on business layer
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@NoArgsConstructor
public class Mapper {

	public static PurchaseOrder fromCommand(UpdatePurchaseOrderCommand command) {

		return ImmutablePurchaseOrder.builder()
									 .id(command.getOrder().getId())
									 .userId(command.getOrder().getUserId())
									 .cost(command.getOrder().getCost())
									 .status(command.getOrder().getStatus())
									 .reference(command.getOrder().getReference())
									 .build();
	}

	public static Payment fromCommand(ApplyPaymentCommand command) {

		return ImmutablePayment.builder()
							   .orderId(command.getOrderId())
							   .userEmail(command.getUserEmail())
							   .userName(command.getUserName())
							   .userContactPhone(command.getUserContactPhone())
							   .userDocument(command.getUserDocument())
							   .userAddress1(command.getUserAddress1())
							   .userAddress2(command.getUserAddress2())
							   .userCity(command.getUserCity())
							   .userState(command.getUserState())
							   .postalCode(command.getPostalCode())
							   .creditCardToken(command.getCreditCardToken())
							   .paymentMethod(command.getPaymentMethod())
							   .deviceSessionId(command.getDeviceSessionId())
							   .transactionType("AUTHORIZATION_AND_CAPTURE")
							   .build();
	}
}
