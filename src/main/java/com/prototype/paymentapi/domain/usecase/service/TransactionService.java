package com.prototype.paymentapi.domain.usecase.service;

import java.util.Optional;

import com.prototype.paymentapi.domain.kernel.command.payment.model.CreditCard;
import com.prototype.paymentapi.domain.kernel.model.Payment;
import com.prototype.paymentapi.domain.kernel.model.PurchaseOrder;
import com.prototype.paymentapi.domain.kernel.model.ResultTransaction;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface TransactionService {

	ResultTransaction apply(Payment payment, PurchaseOrder order, Optional<CreditCard> creditCard);

	ResultTransaction revert(Payment payment);
}
