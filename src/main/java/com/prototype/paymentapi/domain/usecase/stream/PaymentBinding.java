package com.prototype.paymentapi.domain.usecase.stream;

import com.prototype.paymentapi.domain.kernel.event.payment.PaymentEvent;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface PaymentBinding {

	void send(PaymentEvent paymentEvent);
}
