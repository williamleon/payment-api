package com.prototype.paymentapi.domain.usecase.repository;

import java.util.Optional;

import com.prototype.paymentapi.domain.kernel.model.PurchaseOrder;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface PurchaseOrderRepository {

	Optional<PurchaseOrder> findByIdAndStatus(String id, String status);

	PurchaseOrder save(PurchaseOrder purchaseOrder);
}
