package com.prototype.paymentapi.domain.usecase.service;

import com.prototype.paymentapi.domain.kernel.event.EventHandler;
import com.prototype.paymentapi.domain.kernel.event.payment.PaymentEvent;
import com.prototype.paymentapi.domain.usecase.stream.PaymentBinding;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class PaymentEventHandler implements EventHandler<PaymentEvent> {

	private final PaymentBinding paymentBinding;

	/**
	 *
	 * @param event
	 */
	@Override public void handle(final PaymentEvent event) {

		log.info(String.format("handling PaymentEvent for %s", event.getPayment().getId()));
		paymentBinding.send(event);
	}
}
