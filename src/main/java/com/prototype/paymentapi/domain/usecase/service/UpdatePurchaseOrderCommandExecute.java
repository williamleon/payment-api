package com.prototype.paymentapi.domain.usecase.service;

import static com.prototype.paymentapi.domain.kernel.model.mapper.Mapper.fromCommand;

import com.prototype.paymentapi.domain.kernel.command.CommandExecute;
import com.prototype.paymentapi.domain.kernel.command.order.UpdatePurchaseOrderCommand;
import com.prototype.paymentapi.domain.kernel.exception.PurchaseOrderException;
import com.prototype.paymentapi.domain.kernel.model.PurchaseOrder;
import com.prototype.paymentapi.domain.usecase.repository.PurchaseOrderRepository;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class UpdatePurchaseOrderCommandExecute implements CommandExecute<UpdatePurchaseOrderCommand, Either<Throwable, PurchaseOrder>> {

	private static final String MESSAGE = "Getting error updating purchase order %s";
	private final PurchaseOrderRepository purchaseOrderRepository;

	/**
	 * Update purchase order information
	 *
	 * @param command
	 * @return
	 */
	@Override public Either<Throwable, PurchaseOrder> execute(final UpdatePurchaseOrderCommand command) {

		log.info(String.format("Starting update purchase order [%s]..", command.getOrder().getId()));
		return Try.of(() -> command)
				  .map(c -> fromCommand(c))
				  .transform(pos -> Try.of(() -> purchaseOrderRepository.save(pos.get())))
				  .peek(po -> String.format("Update purchase order [%s] successfully", po.getId()))
				  .onFailure(throwable -> log.error(String.format(MESSAGE, command.getOrder().getId()), throwable))
				  .toEither(() -> new PurchaseOrderException(MESSAGE));
	}

}
