package com.prototype.paymentapi.domain.usecase.service;

import static com.prototype.paymentapi.domain.kernel.model.mapper.Mapper.fromCommand;

import com.prototype.paymentapi.domain.kernel.command.CommandExecute;
import com.prototype.paymentapi.domain.kernel.command.payment.ApplyPaymentCommand;
import com.prototype.paymentapi.domain.kernel.event.EventBus;
import com.prototype.paymentapi.domain.kernel.event.payment.ImmutablePaymentEvent;
import com.prototype.paymentapi.domain.kernel.exception.PaymentException;
import com.prototype.paymentapi.domain.kernel.model.ImmutablePayment;
import com.prototype.paymentapi.domain.kernel.model.Payment;
import com.prototype.paymentapi.domain.usecase.repository.PaymentRepository;
import com.prototype.paymentapi.domain.usecase.repository.PurchaseOrderRepository;
import io.vavr.Tuple;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ApplyPaymentCommandExecute implements CommandExecute<ApplyPaymentCommand, Either<Throwable, Payment>> {

	private static final String MESSAGE = "Getting error apply payment for purchase order %s";
	private static final String ORDER_STATUS = "CREATED";
	private final PaymentRepository paymentRepository;
	private final PurchaseOrderRepository purchaseOrderRepository;
	private final TransactionService transactionService;
	private final EventBus bus;

	/**
	 * Load Purchase Order related to given request and then Apply payment transaction, to finalize payment process store the payment
	 * information in DB and emit a event
	 *
	 * @param command
	 * @return
	 */
	@Override public Either<Throwable, Payment> execute(final ApplyPaymentCommand command) {

		log.info(String.format("Starting apply payment for purchase order [%s]..", command.getOrderId()));
		return Try.of(() -> command)


				  .map(c -> Tuple.of(fromCommand(c), c.getCreditCard()))


				  .transform(tuple -> Try.of(() -> transactionService.apply(tuple.get()._1,
																			purchaseOrderRepository
																					.findByIdAndStatus(tuple.get()._1.getOrderId(),
																									   ORDER_STATUS).get(),
																			tuple.get()._2))
										 .peek(t -> log.info(String.format("Apply payment successfully for purchase order [%s], and "
																				   + "generate follow transaction id [%s]",
																		   tuple.get()._1.getOrderId(), t.getTransactionId())))
										 .map(resultTransaction -> Tuple.of(tuple.get()._1, resultTransaction))
							)


				  .map(tuple -> (Payment) ImmutablePayment.copyOf(tuple._1)
														  .withTransactionOrderId(tuple._2.getOrderId())
														  .withTransactionStatus(tuple._2.getState())
														  .withTransactionId(tuple._2.getTransactionId())
					  )

				  .transform(payments -> Try.of(() -> paymentRepository.save(payments.get())))


				  .peek(p -> log.info(String.format("Apply payment successfully for purchase order [%s], transaction id created [%s]",
													p.getOrderId(), p.getTransactionId())))


				  .peek(p -> bus.emit(ImmutablePaymentEvent.builder().payment(p).build()))

				  .onFailure(throwable -> log.error(String.format(MESSAGE, command.getOrderId()), throwable))
				  .toEither(() -> new PaymentException(MESSAGE));
	}
}
