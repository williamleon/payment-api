package com.prototype.paymentapi.domain.usecase.repository;

import java.util.Optional;

import com.prototype.paymentapi.domain.kernel.model.Payment;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface PaymentRepository {

	Optional<Payment> findByOrderId(String orderId);

	Payment save(Payment payment);

}
