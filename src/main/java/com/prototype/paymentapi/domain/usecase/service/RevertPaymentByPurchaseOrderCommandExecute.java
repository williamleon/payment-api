package com.prototype.paymentapi.domain.usecase.service;

import com.prototype.paymentapi.domain.kernel.command.CommandExecute;
import com.prototype.paymentapi.domain.kernel.command.payment.RevertPaymentByPurchaseOrderCommand;
import com.prototype.paymentapi.domain.kernel.event.EventBus;
import com.prototype.paymentapi.domain.kernel.event.payment.ImmutablePaymentEvent;
import com.prototype.paymentapi.domain.kernel.exception.PaymentException;
import com.prototype.paymentapi.domain.kernel.model.ImmutablePayment;
import com.prototype.paymentapi.domain.kernel.model.Payment;
import com.prototype.paymentapi.domain.usecase.repository.PaymentRepository;
import com.prototype.paymentapi.domain.usecase.repository.PurchaseOrderRepository;
import io.vavr.Tuple;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RevertPaymentByPurchaseOrderCommandExecute implements CommandExecute<RevertPaymentByPurchaseOrderCommand, Either<Throwable,
		Payment>> {

	private static final String MESSAGE = "Getting error reverting payment for purchase order %s";

	private final PaymentRepository paymentRepository;
	private final TransactionService transactionService;
	private final EventBus bus;

	/**
	 * Load payment information related to given Purchase order and then revert payment, finally update de payment information y DB and
	 * emit event
	 *
	 * @param command
	 * @return
	 */
	@Override public Either<Throwable, Payment> execute(final RevertPaymentByPurchaseOrderCommand command) {

		log.info(String.format("Starting revert payment for purchase order [%s][%s]..", command.getOrder().getId(),
							   command.getOrder().getStatus()));
		return Try.of(() -> command.getOrder())
				  .filter(purchaseOrder -> purchaseOrder.getStatus().equals("REVERSED"))
				  .peek(po -> log.info(String.format("Retrieving payment related to purchase order [%s] ...", po.getId())))
				  .transform(pos -> Try.of(() -> paymentRepository.findByOrderId(pos.get().getId()).get()))
				  .peek(p -> log.info(String.format("Retrieved payment [%s] ...", p.getId())))
				  .map(p -> ImmutablePayment.copyOf(p).withTransactionType("REFUND"))
				  .transform(ps -> Try.of(() -> transactionService.revert(ps.get()))
									  .peek(t -> log.info(String.format("Revert payment successfully for purchase order [%s]",
																		ps.get().getOrderId())))
									  .map(r -> Tuple.of(ps.get(), r))
							)
				  .map(t -> (Payment) ImmutablePayment.copyOf(t._1).withTransactionStatus(t._2.getState()))
				  .peek(p -> bus.emit(ImmutablePaymentEvent.builder().payment(p).build()))
				  .onFailure(throwable -> log.error(String.format(MESSAGE, command.getOrder().getId()), throwable))
				  .toEither(() -> new PaymentException(MESSAGE));
	}
}
