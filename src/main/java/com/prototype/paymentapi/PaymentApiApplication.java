package com.prototype.paymentapi;

import com.prototype.paymentapi.infrastructure.adapter.inbound.stream.InputBinding;
import com.prototype.paymentapi.infrastructure.adapter.outbound.stream.OutputBinding;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;

@EnableBinding({OutputBinding.class, InputBinding.class})
@SpringBootApplication
public class PaymentApiApplication {

	public static void main(String[] args) {

		SpringApplication.run(PaymentApiApplication.class, args);
	}

}
