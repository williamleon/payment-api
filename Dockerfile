FROM adoptopenjdk/openjdk11
VOLUME /tmp
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} payment-api.jar
ENV JAVA_OPTS="-Xms64m -Xmx1024m"
ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /payment-api.jar"]
